Tor docker container
====================

Docker container with [Tor](https://www.torproject.org/).


Locations
---------

[Source of this image][source] is hosted on Gitlab.com.

If you find any problems, please [post an issue][issues].

The [built image] can be pulled from Docker Hub registry.

[source]: https://gitlab.com/beli-sk/tor-docker
[issues]: https://gitlab.com/beli-sk/tor-docker/issues
[image]: https://hub.docker.com/r/beli/tor


Pull or build
-------------

The image is built automatically and transparently using [Gitlab CI
pipeline][pipeline], pushed to [Docker Hub registry][image] and can be
pulled using command

    docker pull beli/tor

or if you'd prefer to build it yourself from the source repository

    git clone https://gitlab.com/beli-sk/tor-docker.git
    cd tor-docker/
    docker build -t beli/tor .

[pipeline]: https://gitlab.com/beli-sk/tor-docker/pipelines
[image]: https://hub.docker.com/r/beli/tor


Usage
-----

The container exposes port 9050, where Tor is listening for socks connections.

If you would like to supply your own ``torrc`` configuration file from the
host, you can mount it as volume over ``/etc/tor/torrc`` inside the container.

To persist your TOR identity or hidden services, mount a volume over /tor.
