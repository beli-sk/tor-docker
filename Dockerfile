FROM debian:stable
MAINTAINER Michal Belica <code@beli.sk>
EXPOSE 9050
COPY torproject_key.asc /
RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install gnupg dirmngr \
	&& apt-key add /torproject_key.asc && rm /torproject_key.asc \
	&& export CODENAME=`grep ^VERSION_CODENAME= /etc/os-release | cut -d= -f2-` \
	&& echo "deb http://deb.torproject.org/torproject.org $CODENAME main" >> /etc/apt/sources.list \
	&& apt-get update \
	&& DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install tor tor-geoipdb \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*
COPY torrc /etc/tor/
RUN mv /var/lib/tor / && ln -s /tor /var/lib/
VOLUME /tor
CMD ["/usr/bin/tor"]
